# What's a convolutional neural network (CNN)?
> A weighted graph with input and output nodes, *also called neurons*, and it's ordered by *layers*. 

Paper explaining feed-forward networks in a hardware context: [**link**](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&uact=8&ved=2ahUKEwjW56bSgdvhAhVGyqQKHc7ED2AQFjADegQIAhAC&url=http%3A%2F%2Fciteseerx.ist.psu.edu%2Fviewdoc%2Fdownload%3Fdoi%3D10.1.1.18.593%26rep%3Drep1%26type%3Dpdf&usg=AOvVaw00hfDznxGN5Oro58etmtrq).

![Feed-forward network diagram. Each connection has a weight.](https://codeplea.com/public/content/genann_simple.png)

Each node (neuron) in a layer receives input from every node in the previous layer, $l-1$. Each node $j$ computes a weighted sum of all its inputs,

<img src="http://www.sciweavers.org/tex2img.php?eq=net_%7Bj%7D%5E%7B%28l%29%7D%20%3D%20%5Csum_%7Bi%7D%20w_%7Bij%7D%5E%7B%28l%29%7D%20%2A%20o_%7Bi%7D%5E%7B%28l-1%29%7D&bc=White&fc=Black&im=png&fs=12&ff=arev&edit=0" align="center" border="0" alt="net_{j}^{(l)} = \sum_{i} w_{ij}^{(l)} * o_{i}^{(l-1)}" width="189" height="43" />

Then it applies a nonlinear *activation* function to the sum (with a *bias*, $b_{j}^{(l)}$), resulting in an *activation value* of the neuron.
This function usually is the *sigmoid*, 

<div>
	<img src="http://www.sciweavers.org/tex2img.php?eq=o_%7Bj%7D%5E%7B%28l%29%7D%20%3D%20f%5Cleft%28%20net_%7Bj%7D%5E%7B%28l%29%7D%20%2B%20b_%7Bj%7D%5E%7B%28l%29%7D%20%5Cright%29%20&bc=White&fc=Black&im=png&fs=12&ff=arev&edit=0" align="center" border="0" alt="o_{j}^{(l)} = f\left( net_{j}^{(l)} + b_{j}^{(l)} \right) " width="172" height="32" />
</div>

and 

<img src="http://bit.ly/2VbOCFZ" align="center" border="0" alt="f(x) = \frac{1}{1 + e^{-x}}" width="121" height="43" />

But in the example CNNs we will use, the function is the *softplus*, implemented in the so-called **rectified linear unit (ReLU)**, which will be a type of *layer*,

<div>
	<img src="http://www.sciweavers.org/tex2img.php?eq=f%28x%29%20%3D%20log%281%20%2B%20e%5E%7Bx%7D%29&bc=White&fc=Black&im=png&fs=12&ff=arev&edit=0" align="center" border="0" alt="f(x) = log(1 + e^{x})" width="150" height="21" />
</div>

![*Softplus* activation function shown in green.](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Rectifier_and_softplus_functions.svg/1024px-Rectifier_and_softplus_functions.svg.png)


# CAFFE and CMSIS-NN
> We need to get CNN from the web, and **Caffe** (a framework for *deep learning*) provides them from multiple papers in their *model zoo*: [**link**](https://github.com/BVLC/caffe/wiki/Model-Zoo).

* This CNNs come in two type files:
    * `'*.prototxt'` (containing the model info) and `'*.caffemodel'` (containing weight and bias info).

* To ease the readibility of the information, we will use **CMSIS-NN** to convert this files into `'*.network'`.

* **CMSIS-NN** already has some examples: [**link**](https://github.com/openmv/openmv/tree/master/ml/cmsisnn/models).


# .network files

> They have the following information at the start of the file,\
`(char: 1 byte, signed char: 1 byte, int: 4 bytes)`

|	Name			|	Data Type	|	Notes						|
|-------------------|---------------|-------------------------------|
|Type of model		|4-chars		|	Always `CAFE`				|
|Number of layers	|1-int			|								|

**Caffe** layer documentation: [**link**](http://caffe.berkeleyvision.org/tutorial/layers.html).
For each layer, there is the following information,

|	Name			|	Data Type	|	Notes								   								|
|-------------------|---------------|-----------------------------------------------------------------------|
|Type of layer		|1-int			|	`data`, `convolution`, `relu`, <br/>`pooling` or `innerproduct`		|
|Number of layers	|1-int			|																		|
|Shape				|4-int			|	`(n, c, w, h)`, where<br/>`n` is the nº of images as input<br/>`c` is the nº of channels (3 for color)<br/>`w` width for image<br/>`h` height for image		 		   	|

Depending on the layer there is different information. `relu` has no further info. Otherwise:

*	`pooling`:

|	Name			|	Data Type	|	Notes							|
|-------------------|---------------|-----------------------------------|
|Type of pool		|1-int			|	`MAX`,`AVERAGE` or `STOCHASTIC`	|
|Kernel 			|3-int			|	`size`, `pad` and `stride`		|

*	`data`, used for input:

|	Name			|	Data Type	|	Notes							|
|-------------------|---------------|-----------------------------------|
|Mean RGB			|3-int			|	`R`,`G` and `B` from `0-255`	|
|Input scale		|1-int			|									|


*	`convolution`, used for hidden layers:

|	Name			|	Data Type					|	Notes							|
|-------------------|-------------------------------|-----------------------------------|
|Shift				|2-int							|	`left-shift` and `Right-shift`	|
|Kernel 			|3-int							|	`size`, `pad` and `stride`		|
|Number of weights	|1-int							|	 								|
|Weights			|$N_{weights}$-signed char		|	 								|
|Number of bias		|1-int							|	 								|
|Bias				|$N_{bias}$-signed char			|	 								|

*	`innerproduct`, used for output layer:

|	Name			|	Data Type					|	Notes							|
|-------------------|-------------------------------|-----------------------------------|
|Shift				|2-int							|	`left-shift` and `Right-shift`	|
|Number of weights	|1-int							|	 								|
|Weights			|$N_{weights}$-signed char		|	 								|
|Number of bias		|1-int							|	 								|
|Bias				|$N_{bias}$-signed char			|	 								|


