
# import numpy as np
import struct
import os, sys, argparse

caffe_layers = [
    'data', 'convolution', 'relu', 'pooling', 'innerproduct'
]

pool_types = [
    'MAX', 'AVE', 'STOCHASTIC'
]

def show_network(file_name):
    with open(file_name, "rb") as f:
        byte = f.read(4) # CAFE

        print('type of model:', byte.decode('ascii'))

        # number of layers (int 4byte)
        nLayers = struct.unpack("<I", bytearray(f.read(4)))[0]

        print('number of layers:', nLayers)

        for i in range(nLayers):
            lType = struct.unpack("<I", bytearray(f.read(4)))[0]
            lType = caffe_layers[lType] if lType<=4 else 'none'

            print('', 'layer[{0}] type:'.format(i),  lType, end='')

            # layer shape
            layer_shape = f.read(4*4)
            print('', '|', 'shape:', str([struct.unpack('4I', layer_shape)]))

            if lType == 'data':
                mean_r = struct.unpack("<I", bytearray(f.read(4)))[0]
                mean_g = struct.unpack("<I", bytearray(f.read(4)))[0]
                mean_b = struct.unpack("<I", bytearray(f.read(4)))[0]
                input_scale = struct.unpack("<I", bytearray(f.read(4)))[0]

                print('', '', 'mean_r = {0} | mean_g = {1} | mean_b = {2} | input_scale = {3}'.format(mean_r, mean_g, mean_b, input_scale))
            elif lType == 'convolution' or lType == 'innerproduct':
                lshift = struct.unpack("<I", bytearray(f.read(4)))[0]
                rshift = struct.unpack("<I", bytearray(f.read(4)))[0]
                print('', '', 'lshift: {0} | rshift: {1}'.format(lshift, rshift))

                if lType == 'convolution':
                    k_size = struct.unpack("<I", bytearray(f.read(4)))[0]
                    k_pad = struct.unpack("<I", bytearray(f.read(4)))[0]
                    k_stride = struct.unpack("<I", bytearray(f.read(4)))[0]
                    print('', '', 'KERNEL: ', 'size: {0} | pad: {1} | stride: {2}'.format(k_size, k_pad, k_stride))

                # WEIGHTS
                nWeights = struct.unpack("<I", bytearray(f.read(4)))[0]
                print('', '', 'number of weights: ', nWeights)
                for _ in range(nWeights):
                    w = struct.unpack("<B", bytearray(f.read(1)))[0]
                    print('{0: <3}'.format(w), '', end='')
                print('')

                # BIAS
                nBias = struct.unpack("<I", bytearray(f.read(4)))[0]
                print('', '', 'number of bias: ', nBias)
                for _ in range(nBias):
                    b = struct.unpack("<B", bytearray(f.read(1)))[0]
                    print('{0: <3}'.format(b), '', end='')
                print('')
            elif lType == 'pooling':
                poolType = struct.unpack("<I", bytearray(f.read(4)))[0]
                poolType = pool_types[poolType] if poolType<3 else 'none'
                print('', '', 'pool type: ', poolType)

                k_size = struct.unpack("<I", bytearray(f.read(4)))[0]
                k_pad = struct.unpack("<I", bytearray(f.read(4)))[0]
                k_stride = struct.unpack("<I", bytearray(f.read(4)))[0]

                print('', '', 'KERNEL: ', 'size: {0} | pad: {1} | stride: {2}'.format(k_size, k_pad, k_stride))



        while byte:
            byte = f.read(1)
    f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--f', type=str, help='model info')

    args, _ = parser.parse_known_args()
    print('file_name: ', args.f)

    show_network(args.f)
